$(document).ready(function(){
    var initUser = "Player"+Math.floor(Math.random() * 100);
    $('#player').val(initUser);
    var me = function() { return $('#player').val();}

    var winCount = 0;
    var lossCount = 0;
    var tieCount = 0;

    var getRecord = function() {
        return winCount + " wins, " + tieCount + " ties, and " + lossCount + " losses";
    }

    var updateRecord = function(o) {
        var m = me();
        if (o.outcome==='Tie'){
            if ((o.players[0]===m || o.players[1]===m))
                tieCount++;
        } else {
            if (o.winner.player===m)
                winCount++;
            if (o.looser.player===m)
                lossCount++;
        }
        return getRecord();
    }

    var rpsUrl = document.URL
    var rootUrl = rpsUrl.substr(0,rpsUrl.length-4);
    var socket = io.connect(rootUrl);
    socket.on('outcome', function (o) {
        console.log('client recieved:',o);
        $('#choice').text(o.summary);
        $('#outcome').text(o.outcome);

//        $('#record').text(updateRecord(o));
        $('#waiting').text('Waiting for next throw!');
        
        
    });

    socket.on('news', function (d) {
        console.log('client data:',d);
        if ( d.player==me() ){
            $('#waiting').text(`I've chosen ${d.choice}.  Waiting for opponent?`);
        } else {
            $('#waiting').text(`${d.player} has thrown.  What will you throw down?`);
        }
    });

    window.userThrows= function(t) {
        var statement = "I chose " + t;
        $('#choice').text(statement);
        $('#outcome').text("");
        socket.emit('user throw', { choice: t, player: me() });
    }
});