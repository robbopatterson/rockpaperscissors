var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var index = require('./routes/index');
var users = require('./routes/users');
var rps = require('./routes/rps');
var socket_io    = require( "socket.io" );

var app = express();

// Socket.io
var io           = socket_io();
app.io           = io;

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', index);
app.use('/users', users);
app.use('/rps', rps);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

var tieResult = function(t) {
  return {
    summary: "Both players picked " + t1.choice,
    outcome: "Tie",
  };  
}

const tie = "tie", winner = "winner", looser = "looser"; 
let verses = {
  rock : {
    rock : { outcome: tie },
    paper : { outcome: looser, rule: "Paper covers Rock" },
    scissors : { outcome: winner, rule: "Rock smashs Scissors" },
    lizard : { outcome: winner, rule: "Rock crushes Lizard" },
    spock : { outcome: looser, rule: "Spock vaporizes Rock" }
  },
  paper : {
    rock : { outcome: winner, rule: "Paper covers Rock" },
    paper : { outcome: tie },
    scissors : { outcome: looser, rule: "Rock smashs Scissors" },
    lizard : { outcome: looser, rule: "Lizard eats Paper" },
    spock : { outcome: winner, rule: "Paper disproves Spock" }
  },
  scissors : {
    rock : { outcome: looser, rule: "Rock smashs Scissors" },
    paper : { outcome: winner, rule: "Scissors cuts Paper" },
    scissors : { outcome: tie },
    lizard : { outcome: winner, rule: "Scissors decapitates Lizard" },
    spock : { outcome: looser, rule: "Spock smashes Scissors" }
  },
  lizard : {
    rock : { outcome: looser, rule: "Rock crushes Lizard" },
    paper : { outcome: winner, rule: "Lizard eats Paper" },
    scissors : { outcome: looser, rule: "Scissors decapitates Lizard" },
    lizard : { outcome: tie },
    spock : { outcome: winner, rule: "Lizard poisons Spock" }
  },
  spock : {
    rock : { outcome: winner, rule: "Spock vaporizes Rock" },
    paper : { outcome: looser, rule: "Paper disproves Spock" },
    scissors : { outcome: winner, rule: "Spock smashes Scissors" },
    lizard : { outcome: looser, rule: "Lizard poisons Spock" },
    spock : { outcome: tie }
  }
}

let randomTieExplicative = ["Facinating!", "Unlikely indeed.", "Quite Illogical.", "Wholly Unexpected."];
function outcomeOfP1versesP2( p1, p2 ) {
  let o = verses[p1.toLowerCase()][p2.toLowerCase()];
  if (o.outcome===tie) {
    o.rule = `Tie. Both players selected ${p1}. ` 
            + randomTieExplicative[Math.floor(Math.random() * randomTieExplicative.length)];
  }
  return o;
}

var outcome = function(t1,t2) {
  let out1vs2 = outcomeOfP1versesP2(t1.choice,t2.choice);
  let out = {
    summary : out1vs2.rule
  };
  switch(out1vs2.outcome) {
    case winner:
      out.outcome = `${t1.player} wins`;
      return out;
    case looser:
      out.outcome = `${t2.player} wins`;
      return out;
    default:
  }
  out.outcome = `No winner`;
  return out;
}



var lastThrow;
io.on('connection', function (socket) {
    socket.on('user throw', function (data) {
      console.log('data',data);
      if (lastThrow) {
        if (lastThrow.player===data.player) {
          io.emit('news', data);
          lastThrow = data; // Player changed his mind
        } else {
          io.emit('outcome', outcome(lastThrow, data));
          lastThrow = null;
        }
      } else {
        lastThrow = data;
        io.emit('news', data);
      }
    });
});

module.exports = app;
